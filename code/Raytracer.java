package code;


import java.time.LocalDateTime;
import java.time.Duration;

public class Raytracer {

	//Parameter to print out addition timing info
	private static boolean printTimes = false;
	
	//Time variables
	private static LocalDateTime startSetup, stopSetup, stopRender, stopTime;
	private static Duration timeSetup, timeRender, timeTaken;
	
	public static void main(String[] args) {
		//Set local directories for input/output files
		String drvdir = "";
		String outdir = "";
		
		//Read the driver file
		if (printTimes) {System.out.print("Setting Up... ");}
		startSetup = LocalDateTime.now();
		String infile = drvdir + args[0];
		DriverSpecs mySpecs = new DriverSpecs(infile);

		//Setup the image
		Image myImage = new Image(mySpecs);
		if (printTimes) {
			stopSetup = LocalDateTime.now();
			timeSetup = Duration.between(startSetup,  stopSetup);
			System.out.print(timeSetup.getSeconds() + "s\n");
		}
		
		//Render the image
		System.out.print("Rendering...");
		myImage.render();
		if (printTimes) {
			stopRender = LocalDateTime.now();
			timeRender = Duration.between(stopSetup, stopRender);
			System.out.print(timeRender.getSeconds() + "s\n");
		}
		
		//Write the output file
		String outfile = outdir + args[1];
		myImage.write(outfile);
	
		stopTime = LocalDateTime.now();
		timeTaken = Duration.between(startSetup, stopTime);
		System.out.println("Finished Processing, Created Image: " + outfile);
		System.out.println("Time Taken (s): " + timeTaken.getSeconds());
	}
}