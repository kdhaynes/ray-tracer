package code;


import java.io.*;
import java.util.StringTokenizer;
import org.apache.commons.math3.linear.*;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.FastMath;
import java.util.ArrayList;

public class ModelObj {

	//Parameter to allow using file vector normals
	private boolean allowFileN = false;
	
	//Directory where model information is located
	private String modelFileDir = "./models/";
	
	//Driver file information
	private double scale, theta, vsmooth;
	private Vector3D Uv, Vv, Wv, Tv;
	private String modelFile;
	
	//Model vertex information
	private boolean useFileN;
	private int lindx, sindx, smatch;
	private int vindx, vnindx, vtindx;
	private int numv, numvn, numvt;
	private RealMatrix ObjMatrix, ObjNMatrix, ObjTexMatrix;
	private RealMatrix ObjTMatrix, ObjTNMatrix;
	
	//Model face information
	private int numf;
	private ArrayList<Integer> face, faceN, faceT;
	private ArrayList<ArrayList<Integer>> ObjFaces, ObjFacesN, ObjFacesT;
	private ArrayList<Vector3D> ObjFaceAv;
	private ArrayList<Integer> ObjFaceMat;
	private ArrayList<double[][]> ObjFaceMM;
	private ArrayList<Vector3D> ObjFaceVN1, ObjFaceVN2, ObjFaceVN3;
	private ArrayList<RealMatrix> ObjFaceTex;
	
	//Adjustment matrices
	private RealMatrix RT, SM, TM;
	private RealMatrix TransM;
	
	//Material information
	private int matRef;
    private Material ObjMat;
	
	//Refraction information
	private double a, b, etar, radsq, WdotN;
	private double beta, gamma, tval;
	private Vector3D Tvr, Tvr2;
	private DecompositionSolver solver;
	private RealMatrix coefs;
	private RealVector consts, myD, solution;
	
	//Constructor
	public ModelObj() {
		useFileN = false;
		ObjMat = new Material();
        ObjFaces = new ArrayList<ArrayList<Integer>>();
        ObjFacesN = new ArrayList<ArrayList<Integer>>();
        ObjFacesT = new ArrayList<ArrayList<Integer>>();
        ObjFaceAv = new ArrayList<Vector3D>();
        ObjFaceMat = new ArrayList<Integer>();
        ObjFaceMM = new ArrayList<double[][]>();
        ObjFaceVN1 = new ArrayList<Vector3D>();
        ObjFaceVN2 = new ArrayList<Vector3D>();
        ObjFaceVN3 = new ArrayList<Vector3D>();
        ObjFaceTex = new ArrayList<RealMatrix>();
	}

	//Set variables
	public void setWv(double wx, double wy, double wz) {
		Wv = new Vector3D(wx, wy, wz);
		Wv = Wv.normalize();
	}
	
	public void setTv(double tx, double ty, double tz) {
		Tv = new Vector3D(tx, ty, tz);
	}
	
	public void setTheta(double theta) {
		this.theta = theta;
	}
	
	public void setScale(double scale) {
		this.scale = scale;
	}
	
	public void setVsmooth(double vsmooth) {
		this.vsmooth = vsmooth;
	}
	
	//Get variables
	public int getNumF() {
		return this.numf;
	}
	
	public ArrayList<Integer> getFace(int findx) {
		return ObjFaces.get(findx);
	}
	
	public int getFaceMat(int findx) {
		return ObjFaceMat.get(findx);
	}
	
	public double[][] getFaceMM(int findx) {
		return ObjFaceMM.get(findx);
	}
	
	public Vector3D getFaceN(int findx, double beta, double gamma) {
		Vector3D myN = ObjFaceVN1.get(findx).scalarMultiply(1 - beta - gamma);
		myN = myN.add(ObjFaceVN2.get(findx).scalarMultiply(beta));
		myN = myN.add(ObjFaceVN3.get(findx).scalarMultiply(gamma));
		return myN.normalize();
	}
	
	public double[] getFaceYv(int findx, Vector3D L) {
		double[] Yv = ObjFaceAv.get(findx).subtract(L).toArray();
		return Yv;
	}
	
	public Material getMaterial() {
		return ObjMat;
	}
	
	public Vector3D getVertex(int vindx) {
		double[] myCol = ObjTMatrix.getColumn(vindx);
		return new Vector3D(myCol[0], myCol[1], myCol[2]);
	}
	
	public Vector3D getVertexN(int vindx) {
		double[] myCol = ObjTNMatrix.getColumn(vindx);
		return new Vector3D(myCol[0], myCol[1], myCol[2]);
	}
		
	public double getVsmooth() {
		return vsmooth;
	}
	
	//Read in the model object file
	public void readObj(String file) {
		modelFile = modelFileDir + file;
		try {			
			BufferedReader read = new BufferedReader(new FileReader(modelFile));
			String line = null;
			while ( (line = read.readLine()) != null) {
				StringTokenizer tokens = new StringTokenizer(line);
				String first = tokens.nextToken();
				switch (first) {
				case ("v"):
					numv ++;
					break;
				case ("vn"):
					numvn ++;
					break;
				case ("vt"):
					numvt ++;
					break;
				case ("f"):
					numf ++;
				}
			} 
			read.close();
			
	        ObjMatrix = new Array2DRowRealMatrix(4,numv);
	        if (numvn > 0) {
	        	ObjNMatrix = new Array2DRowRealMatrix(4,numvn);
	        }
	        if (numvt > 0) {
	        	ObjTexMatrix = new Array2DRowRealMatrix(2,numvt);
	        }
	        
			int mycol = 0;
			int mycolvn = 0;
			int mycolvt = 0;
			read = new BufferedReader(new FileReader(modelFile));
			while ( (line = read.readLine()) != null) {
				StringTokenizer tokens = new StringTokenizer(line);
				String first = tokens.nextToken();
				switch (first) {
				case ("v"):
					ObjMatrix.setEntry(0, mycol, Double.valueOf(tokens.nextToken()));
					ObjMatrix.setEntry(1, mycol, Double.valueOf(tokens.nextToken()));
					ObjMatrix.setEntry(2, mycol,  Double.valueOf(tokens.nextToken()));
					mycol++;
					break;
				case ("vn"):
					ObjNMatrix.setEntry(0,  mycolvn,  Double.valueOf(tokens.nextToken()));
					ObjNMatrix.setEntry(1,  mycolvn,  Double.valueOf(tokens.nextToken()));
					ObjNMatrix.setEntry(2,  mycolvn,  Double.valueOf(tokens.nextToken()));
					mycolvn++;
					break;
				case ("vt"):
					ObjTexMatrix.setEntry(0,  mycolvt,  Double.valueOf(tokens.nextToken()));
				    ObjTexMatrix.setEntry(1,  mycolvt,  Double.valueOf(tokens.nextToken()));
				    mycolvt++;
				    break;
				case ("f"):
					face = new ArrayList<Integer>();
		        	faceN = new ArrayList<Integer>();
		        	faceT = new ArrayList<Integer>();
					
					while (tokens.hasMoreTokens()) {
						String next = tokens.nextToken();
						smatch = (next.length() - next.replace("/",  "").length());
						if (smatch < 1) {
							vindx = Integer.valueOf(next) - 1;
							vtindx = -1;
							vnindx = -1;
						} else {
							sindx = next.indexOf("/");
							vindx = Integer.valueOf(next.substring(0,sindx)) - 1;
							if (smatch == 1){
						        if (numvt > 1) {
						        	vtindx = Integer.valueOf(next.substring(sindx+1,next.length()))-1; 
						        	vnindx = -1;
						        } else {
						        	vtindx = -1;
						        	vnindx = Integer.valueOf(next.substring(sindx+1,next.length()))-1;
						        }
							} else {
								sindx = next.indexOf("/");
								lindx = next.lastIndexOf("/");
								if (lindx-sindx > 1) {
									vtindx = Integer.valueOf(next.substring(sindx+1,lindx-sindx+1))-1;
									vnindx = Integer.valueOf(next.substring(lindx+1,next.length()))-1;
								} else {
									vtindx = -1;
									vnindx = Integer.valueOf(next.substring(lindx+1,next.length()))-1;
								}
							}
						}
						face.add(vindx);
						faceT.add(vtindx);
						faceN.add(vnindx);
						
					}
					ObjFaces.add(face);
					ObjFacesN.add(faceN);
					ObjFacesT.add(faceT);
					ObjFaceMat.add(matRef);
					break;
				case ("mtllib"):
				    ObjMat.readMat(modelFileDir, tokens.nextToken());
					break;
				case ("usemtl"):
				    matRef = ObjMat.getMatRef(tokens.nextToken());
					break;
				default:
					break;
				}
			}
			read.close();
			
			//Set the 4th line of object vertex matrix to 1s
			for (int j=0; j<numv; j++) {
				ObjMatrix.setEntry(3, j, 1);
			}
			
			//Set the 4th line of object vertex normal matrix to 1s
			for (int j=0; j<numvn; j++) {
				ObjNMatrix.setEntry(3, j, 1);
			}
			
			//Set flag for using file normals
			if ((numvn > 0) && (mycolvn == numvn)) {
				useFileN = true;
			}
		} 
		
		catch (IOException e) {
			String errMessage = "Missing Object File: " + file;
			System.err.println(errMessage);
			System.exit(0);
			//e.printStackTrace();
		}
	}
	
	//Transform the model object using axis-angle rotation
	public void transformObj() {
		
		//Create rotation matrix
		rotateMatrix();
		
		//Create scaling matrix
		scaleMatrix();
		
		//Create translation matrix
		translateMatrix();
		
		//Create transformation matrix
		transformMatrix();
		
		//Transform the model object
		transformObject();
		
		//Calculate vertex normals
		if (allowFileN && useFileN) {
			getVNormals();
		} else {
			calcVNormals();
		}
		
		//Create a matrix for ray intersections
		createMMatrix();
		
	}
	
	//Create the rotation matrix
	private void rotateMatrix() {
		//Find vector Mv not parallel to Wv
		double[] MvDbl = {FastMath.abs(Wv.getX()), FastMath.abs(Wv.getY()),
							FastMath.abs(Wv.getZ())};
		int iref = 0;
		double dval = 999.;
		for (int i=0;i<3;i++ ) {
			if (MvDbl[i] <= dval) {
				iref = i;
				dval = MvDbl[i];
			}
		}
		MvDbl[iref] = 1.0;
		Vector3D Mv = new Vector3D(MvDbl);
		
		//Define Uv as cross product of Wv and Mv
		Uv = Wv.crossProduct(Mv);
		Uv = Uv.normalize();
		
		//Let Vv be orthogonal to Uv and Wv
		Vv = Wv.crossProduct(Uv);
		
		//Put these results into a matrix
		RealMatrix RM = new Array2DRowRealMatrix(3,3);
		RM.setRow(0, Uv.toArray());
		RM.setRow(1, Vv.toArray());
		RM.setRow(2, Wv.toArray());
		RealMatrix RMt = RM.transpose();
		
		//Check that RM * RMt == Identity
		//RealMatrix RMRMt = RM.multiply(RMt);
		//System.out.println(RMRMt.toString());
		
		//Rotate by theta about the z-axis
		double arad = (theta / 180.) * FastMath.PI;
		double ca = FastMath.cos(arad);
		double sa = FastMath.sin(arad);
		RealMatrix RMz = new Array2DRowRealMatrix(3,3);
		RMz.setEntry(0, 0, ca);
		RMz.setEntry(0, 1, -sa);
		RMz.setEntry(1, 0, sa);
	    RMz.setEntry(1, 1, ca);
	    RMz.setEntry(2, 2, 1.0);
	    
	    //Put the 2 parts together
	    RealMatrix RTtemp = RMt.multiply(RMz.multiply(RM));
	    this.RT = new Array2DRowRealMatrix(4,4);
	    for (int i=0; i<3; i++) {
	    	for (int j=0; j<3; j++) {
	    		RT.setEntry(i, j, RTtemp.getEntry(i, j));
	    	}
	    }
	    RT.setEntry(3, 3, 1.);
	}
	
	//Create the scaling matrix
	private void scaleMatrix() {
		SM = new Array2DRowRealMatrix(4,4);
		SM.setEntry(0, 0, scale);
		SM.setEntry(1, 1, scale);
		SM.setEntry(2, 2, scale);
		SM.setEntry(3, 3, 1.0);
	}
	
	//Create the translation matrix
	private void translateMatrix() {
		TM = new Array2DRowRealMatrix(4,4);
		TM.setEntry(0,0,1.);
		TM.setEntry(0,3,Tv.getX());
		TM.setEntry(1,1,1.);
		TM.setEntry(1,3,Tv.getY());
		TM.setEntry(2,2,1.);
		TM.setEntry(2,3,Tv.getZ());
        TM.setEntry(3,3,1.);
	}
	
	//Create the transformation matrix
	private void transformMatrix() {
		TransM = TM.multiply(SM.multiply(RT));
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				if (FastMath.abs(TransM.getEntry(i,j)) < 1.e-10) {
					TransM.setEntry(i, j, FastMath.abs(TransM.getEntry(i, j)));
				}
			}
		}
	}
	
	private void transformObject() {
		ObjTMatrix = TransM.multiply(ObjMatrix);
		if (numvn > 0) {
			ObjTNMatrix = TransM.multiply(ObjNMatrix);
		}
	}
	
	//Method to create an MM matrix for each face.
	//Assumes all faces are triangles.
	private void createMMatrix() {
		for (int f=0; f<numf; f++) {
			ArrayList<Integer> myFace = ObjFaces.get(f);
			Vector3D Av = this.getVertex(myFace.get(0));
			Vector3D Bv = this.getVertex(myFace.get(1));
			Vector3D Cv = this.getVertex(myFace.get(2));
			ObjFaceAv.add(Av);
						
			double[][] MM = new double[3][3];
			double[] AAv = Av.subtract(Bv).toArray();
			double[] BBv = Av.subtract(Cv).toArray();
			MM[0][0] = AAv[0];
			MM[1][0] = AAv[1];
	        MM[0][0] = AAv[0];
		    MM[1][0] = AAv[1];
		    MM[2][0] = AAv[2];
		    MM[0][1] = BBv[0];
		    MM[1][1] = BBv[1];
		    MM[2][1] = BBv[2];
		    ObjFaceMM.add(MM);
		}
	}
	
	//Calculate average vertex normals
	private void calcVNormals() {
		ArrayList<Vector3D> trueFN = new ArrayList<Vector3D>();
		ArrayList<Integer> myFace = new ArrayList<Integer>();
		Vector3D Av, Bv, Cv;
		Vector3D locVN1, locVN2, locVN3;
		int myFacev1, myFacev2, myFacev3;
		double cosAngle, degAngle;
		
		for (int f=0; f<numf; f++) {
			myFace = ObjFaces.get(f);
			Av = this.getVertex(myFace.get(0));
			Bv = this.getVertex(myFace.get(1));
			Cv = this.getVertex(myFace.get(2));
			locVN1 = Bv.subtract(Av);
			locVN2 = Cv.subtract(Av);
			trueFN.add(locVN1.crossProduct(locVN2).normalize());
		}
		
		for (int f=0; f<numf; f++) {
			myFace = ObjFaces.get(f);
			myFacev1 = myFace.get(0);
			myFacev2 = myFace.get(1);
			myFacev3 = myFace.get(2);
			locVN1 = trueFN.get(f);
			locVN2 = trueFN.get(f);
			locVN3 = trueFN.get(f);
			
			for (int ff=0; ff<numf; ff++) {
				if (ff != f) {		
					ArrayList<Integer> compFace = ObjFaces.get(ff);
					if ((compFace.get(0) == myFacev1) || (compFace.get(1) == myFacev1) ||
						(compFace.get(2) == myFacev1)) {
						cosAngle = trueFN.get(f).dotProduct(trueFN.get(ff));
						degAngle = Math.toDegrees(Math.acos(cosAngle));
						if ((degAngle > -vsmooth) && (degAngle < vsmooth)) {
							locVN1 = locVN1.add(trueFN.get(ff));
						}
					}
					
					if ((compFace.get(0) == myFacev2) || (compFace.get(1) == myFacev2) ||
						(compFace.get(2) == myFacev2)) {
						cosAngle = trueFN.get(f).dotProduct(trueFN.get(ff));
						degAngle = Math.toDegrees(Math.acos(cosAngle));
						if ((degAngle > -vsmooth) && (degAngle < vsmooth)) {
							locVN2 = locVN2.add(trueFN.get(ff));
						}
					}
					
					if ((compFace.get(0) == myFacev3) || (compFace.get(1) == myFacev3) ||
						(compFace.get(2) == myFacev3)) {
						cosAngle = trueFN.get(f).dotProduct(trueFN.get(ff));
						degAngle = Math.toDegrees(Math.acos(cosAngle));
						if ((degAngle > -vsmooth) && (degAngle < vsmooth)) {
							locVN3 = locVN3.add(trueFN.get(ff));
						}
					}
				}
			}
			ObjFaceVN1.add(locVN1.normalize());
			ObjFaceVN2.add(locVN2.normalize());
			ObjFaceVN3.add(locVN3.normalize());
		}
	}

	//Get vertex normals from file information
	private void getVNormals() {
		ArrayList<Integer> myFaceN = new ArrayList<Integer>();
		
		for (int f=0; f<numf; f++) {
			myFaceN = ObjFacesN.get(f);
			ObjFaceVN1.add(this.getVertexN(myFaceN.get(0)).normalize());
			ObjFaceVN2.add(this.getVertexN(myFaceN.get(1)).normalize());
			ObjFaceVN3.add(this.getVertexN(myFaceN.get(2)).normalize());
		}
	}
	
	//Refraction
	public Vector3D refractT(Vector3D W, Vector3D N, double eta1, double eta2) {
			if (eta2 != 0.0) {
				etar = eta1 / eta2;
			} else {
				return null;
			}
			
			WdotN = W.dotProduct(N);
			radsq = (etar*etar) * ((WdotN*WdotN) - 1) + 1;
			if (radsq < 0.0) {
				return null;
			} else {
				a = -etar;
				b = (etar * WdotN) - FastMath.sqrt(radsq);
				return W.scalarMultiply(a).add(N.scalarMultiply(b));
			}
		}
		
		
	public Ray refractR(Vector3D W, Vector3D N, Vector3D pt, int findx,
					    double etaIn, double etaOut) {
		
		//Calculate refraction ray inside object
		Tvr = refractT(W, N, etaOut, etaIn).normalize();
		
		//Now with this refraction vector, find the exit ray
		if (Tvr == null) {
			return null;
		} else {
			return refractExit(pt, Tvr, findx, etaIn, etaOut);
		}
	}
	
	
	private Ray refractExit(Vector3D L, Vector3D D, int findxOn, double etaIn, double etaOut) {
		Vector3D exitPt = null;
		Vector3D exitN = null;
		boolean myHit = false;
		double best_tval = Double.POSITIVE_INFINITY;
		
		for (int f=0; f<numf; f++) {
			if (f != findxOn) {
				coefs = new Array2DRowRealMatrix(ObjFaceMM.get(f), false);
				myD = new ArrayRealVector(D.toArray());
				coefs.setColumnVector(2, myD);
				solver = new LUDecomposition(coefs).getSolver();
				if (solver.isNonSingular()){
					consts = new ArrayRealVector(getFaceYv(f, L), false);
				    solution = solver.solve(consts);
				    beta = solution.getEntry(0);
				    gamma = solution.getEntry(1);
				    tval = solution.getEntry(2);
				
				    if ((tval > 0.00001) && (beta > -0.00001) && (gamma > -0.00001) 
						&& ((beta + gamma) <= 1.00001)) {
				    
				    	if (tval < best_tval) {
				    		myHit = true;
				    		exitPt = L.add(D.scalarMultiply(tval));
				    		exitN = getFaceN(f, beta, gamma);
				    		if (exitN.dotProduct(L.subtract(exitPt)) < 0.) {
				    			exitN = exitN.negate();
				    		}
				    		best_tval = tval;
				    	}
				    } 
				}
			}
		}
		
		if (myHit) {
			Tvr2 = refractT(D.negate(), exitN, etaIn, etaOut);
			if (Tvr2 != null) {
				return new Ray(exitPt, Tvr2);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	
	// Texture
	public void transformTexture() {
		ArrayList<Integer> myFaceIs, myTextureIs;
		Vector3D Av, Bv, Cv;
		RealMatrix worldPtsInverse, solveM;
		RealMatrix worldPts = new Array2DRowRealMatrix(3, 3);
		RealMatrix texturePts = new Array2DRowRealMatrix(2, 3);
		
		if (numvt < 1) {
			return;
		} else {
			for (int f=0; f<numf; f++) {
				myFaceIs = ObjFaces.get(f);
				Av = this.getVertex(myFaceIs.get(0));
				Bv = this.getVertex(myFaceIs.get(1));
				Cv = this.getVertex(myFaceIs.get(2));
				worldPts.setColumn(0, Av.toArray());
				worldPts.setColumn(1, Bv.toArray());
				worldPts.setColumn(2, Cv.toArray());
				solver = new LUDecomposition(worldPts).getSolver();
				if (solver.isNonSingular()) {
					worldPtsInverse = solver.getInverse();
			
					myTextureIs = ObjFacesT.get(f);
					texturePts.setColumn(0, ObjTexMatrix.getColumn(myTextureIs.get(0)));
					texturePts.setColumn(1, ObjTexMatrix.getColumn(myTextureIs.get(1)));
					texturePts.setColumn(2, ObjTexMatrix.getColumn(myTextureIs.get(2)));
				
					solveM = texturePts.multiply(worldPtsInverse);
				} else {
					solveM = new Array2DRowRealMatrix(2, 3);
				}
				
				ObjFaceTex.add(solveM);
			}
		}  
	}
	
	public int[] getTexturePt(int findx, double[] hitPt, int nimx, int nimy) {
		RealMatrix textTransform = ObjFaceTex.get(findx);
		RealMatrix hitPtRM = new Array2DRowRealMatrix(hitPt);
		RealMatrix hitTexturePt = textTransform.multiply(hitPtRM);
		
		double[] hitTexturePtArray = hitTexturePt.getColumn(0);
		int hitx = (int)Math.max(0,Math.min(nimx-1,Math.round(hitTexturePtArray[0]*nimx)));
		int hity = (int)Math.max(0,Math.min(nimx-1,nimy-Math.round(hitTexturePtArray[1]*nimy)));
		//int hitx = (int)Math.max(0,Math.min(nimx-1,Math.round(hitTexturePtArray[0]*nimx)));
		//int hity = nimy - (int)Math.max(0,Math.min(nimx-1,Math.round(hitTexturePtArray[1]*nimy)));
		return new int[] {hitx,hity};
	}
	
}
