package code;


import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.FastMath;

public class Sphere {

	//Sphere properties
	public double alpha, radius, Ni;
	public Vector3D C, ka, kd, ks, kr, ko;
	
	//Refraction variables
	private double a, b, etar, radsq, WdotN;
	private Vector3D exitPt, exitN, T1, T2;
	
	//Initialization
	public Sphere() {}
	
	//Set routines
	public void setC(double cx, double cy, double cz) {
		this.C = new Vector3D(cx, cy, cz);
	}
	
	public void setka(double kax, double kay, double kaz) {
		this.ka = new Vector3D(kax, kay, kaz);
	}
	
	public void setkd(double kdx, double kdy, double kdz) {
		this.kd = new Vector3D(kdx, kdy, kdz);
	}
	
	public void setks(double ksx, double ksy, double ksz) {
		this.ks = new Vector3D(ksx, ksy, ksz);
	}
	
	public void setkr(double krx, double kry, double krz) {
		this.kr = new Vector3D(krx, kry, krz);
	}
	
	public void setko(double kox, double koy, double koz) {
		if (this.Ni < 0.001) {
			this.ko = new Vector3D(1.0, 1.0, 1.0);
		} else {
			this.ko = new Vector3D(kox, koy, koz);
		}
	}
	
	//Refraction Vector Through Sphere
	public Vector3D refractT(Vector3D W, Vector3D N, double eta1, double eta2) {
		if (eta2 != 0.0) {
			etar = eta1 / eta2;
		} else {
			return null;
		}
		
		WdotN = W.dotProduct(N);
		radsq = (etar*etar) * ((WdotN*WdotN) - 1) + 1;
		if (radsq < 0.0) {
			return null;
		} else {
			a = -etar;
			b = (etar * WdotN) - FastMath.sqrt(radsq);
			return W.scalarMultiply(a).add(N.scalarMultiply(b));
		}
	}
	
	//Refraction Sphere Exit Ray
	public Ray refractR(Vector3D W, Vector3D pt, double etaIn, double etaOut) {
		T1 = refractT(W, (pt.subtract(C)).normalize(), etaOut, etaIn);
		if (T1 == null) {
			return null;
		} else {
			exitPt = pt.add(T1.scalarMultiply(2 * (C.subtract(pt)).dotProduct(T1)));
			exitN = C.subtract(exitPt).normalize();
			T2 = refractT(T1.negate(), exitN, etaIn, etaOut);
			return new Ray(exitPt, T2);
		}
	}

}
