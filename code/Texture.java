package code;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import org.apache.commons.io.FilenameUtils;

public class Texture {
	
	public static Boolean printRGBs = false;
	public String textureName;
	public BufferedImage textureImage;
	public int nimx, nimy;
	
	// Constructor
	public Texture(String filename) {
		textureName = filename;
		if (filename != "") {
			readJPG(filename);
		} 
	}

	// Read Texture JPG File
	private void readJPG(String filename) {
		String fext = FilenameUtils.getExtension(filename);
		if ((fext.equals("jpg")) || (fext.equals("jpeg"))) {
			//System.out.println("Using Texture: " + filename);
			try {
				textureImage = ImageIO.read(new File(filename));
				nimx = textureImage.getWidth();
				nimy = textureImage.getHeight();
				// Print the ARGB values
				if (printRGBs) {
					for (int i = 0; i<nimy; i++) {
						for (int j=0; j<nimx; j++) {
							int pixel = textureImage.getRGB(j, i);
							printPixelARGB(pixel);
						}
					}
				}
			} catch (IOException e) {
				System.err.println("Error Reading Texture Image: " + filename);
				System.exit(0);
			}
		} else {
			System.err.println("Only Expecting JPG Textures!");
			System.exit(0);
		}
	}
	
	private void printPixelARGB(int pixel) {
		int alpha = (pixel >> 24) & 0xff;
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		System.out.println("ARGB: " + alpha + ", " + red + ", " + green + ", " + blue);
	}
	
	public Vector3D getKo(int col, int row) {
		//System.out.println("TEXTURE: " + textureName);
		int pixel = textureImage.getRGB(col, row);  //, row);  // col);
		int alpha = (pixel >> 24) & 0xff;
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		double dalpha = alpha/255.;
		double dred = red/255.;
		double dgreen = green/255.;
		double dblue = blue/255.;
		return new Vector3D(dalpha*dred, dalpha*dgreen, dalpha*dblue);
	}
	
}
