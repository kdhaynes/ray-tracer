package code;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class Light {

	public Vector3D P;
	public Vector3D E;
	
	public Light() {}
	
	public void setP(double px, double py, double pz) {
		this.P = new Vector3D(px, py, pz);
	}
	
	public void setE(double ex, double ey, double ez) {
		this.E = new Vector3D(ex, ey, ez);
	}

}
