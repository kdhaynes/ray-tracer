package code;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class Camera {

	public int width, height;
	public double near, left, right, top, bottom;
	public Vector3D EV, LV, UP;
	public Vector3D UV, VV, WV;

	public Camera() {}

	public void setEV(double eyex, double eyey, double eyez) {
		this.EV = new Vector3D(eyex, eyey, eyez);
	}
	
	public void setLV(double lookx, double looky, double lookz) {
		this.LV = new Vector3D(lookx, looky, lookz);
	}
	
	public void setUP(double upx, double upy, double upz) {
		this.UP = new Vector3D(upx, upy, upz);
	}
	
	public void setNear(double near) {
		this.near = near;
	}
	
	public void setBounds(double bnd0, double bnd1, double bnd2, double bnd3) {
		this.left = bnd0;
		this.right = bnd1;
		this.bottom = bnd2;
		this.top = bnd3;
	}
	
	public void setRes(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public void setUVW() {
		WV = EV.subtract(LV).normalize();
		UV = UP.crossProduct(WV).normalize();
		VV = WV.crossProduct(UV);
	}
}

