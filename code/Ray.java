package code;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.*;
import java.lang.Math;

public class Ray {

	public Vector3D L;
	public Vector3D D;
	public double best_st;
	public Sphere best_sph;
	public Vector3D best_spt;
	public double best_mt;
	public int best_mfindx, best_mmat;
	public ModelObj best_mod;
	public Vector3D best_mpt;
	public Vector3D best_mptN;
	public boolean inObj;
	
	//Sphere Test Variables
	private double csq, disc, tval, v; 
	private Vector3D Tv;
	
	//Model Test Variables
	private boolean myHit;
	private double beta, gamma;
	private DecompositionSolver solver;
	private RealMatrix coefs;
	private RealVector consts, myD, solution;
	
	public Ray(Vector3D L, Vector3D D) {
		this.L = L;
		this.D = D.normalize();
		best_st = Double.POSITIVE_INFINITY;
		best_sph = null;
		best_spt = null;
		best_mt = Double.POSITIVE_INFINITY;
		best_mod = null;
		best_mpt = null;
		best_mptN = null;
		inObj = false;
	}
	
	public boolean sphereTest(Sphere mySphere) {
		Tv = mySphere.C.subtract(L);
		v = Tv.dotProduct(D);
		csq = Tv.dotProduct(Tv);
		disc = mySphere.radius*mySphere.radius - (csq - (v*v));
		if (disc > 0) {
			tval = v - Math.sqrt(disc);
			if ((tval < best_st) && (tval > 0.00001)) {
				best_st = tval;
				best_sph = mySphere;
				best_spt = L.add(D.scalarMultiply(tval));
			}
			return true;
		} else {
			return false;
		}
	}
	
	public boolean modelTest(ModelObj myModel) {
		myHit = false;
		for (int f=0; f<myModel.getNumF(); f++) {
			coefs = new Array2DRowRealMatrix(myModel.getFaceMM(f), false);
			myD = new ArrayRealVector(D.toArray());
			coefs.setColumnVector(2, myD);
			solver = new LUDecomposition(coefs).getSolver();
			if (solver.isNonSingular()){
				consts = new ArrayRealVector(myModel.getFaceYv(f, L), false);
			    solution = solver.solve(consts);
			    beta = solution.getEntry(0);
			    gamma = solution.getEntry(1);
			    tval = solution.getEntry(2);
			
			    if ((tval > 0.00001) && (beta > -0.00001) && (gamma > -0.00001) 
					&& ((beta + gamma) <= 1.00001)) {
			    
			    	myHit = true;
			    	if (tval < best_mt) {
			    		best_mfindx = f;
			    		best_mmat = myModel.getFaceMat(f);
			    		best_mt = tval;
			    		best_mod = myModel;
			    		best_mpt = L.add(D.scalarMultiply(tval));
			    		best_mptN = myModel.getFaceN(f, beta, gamma);
			    	}
			    } 
			}
		}
		return myHit;
	}

}
