package code;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class DriverSpecs {

	private Camera tCam;
	private ArrayList<Light> tLights;
	private ArrayList<ModelObj> tModels;
	private ArrayList<Sphere> tSpheres;
	private Vector3D tAmbient;
	private int tRecLev;
	
	public DriverSpecs(String filename) {
		
		tCam = new Camera();
		tLights = new ArrayList<Light>();
		tSpheres = new ArrayList<Sphere>();
		tModels = new ArrayList<ModelObj>();
		tAmbient = new Vector3D(0,0,0);
		
		try {
			BufferedReader read = new BufferedReader(new FileReader(filename));
			String line = null;
			while ( (line = read.readLine()) != null) {
				StringTokenizer tokens = new StringTokenizer(line);
				String first = "";
				if (tokens.hasMoreTokens()) {
					first = tokens.nextToken();
				} 
				switch (first) {
				case ("eye"):
					tCam.setEV(Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()));
					break;
				case ("look"):
					tCam.setLV(Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()));
					break;
				case ("up"):
					tCam.setUP(Double.valueOf(tokens.nextToken()),
							   Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()));
					break;
				case ("d"):
					tCam.setNear(Double.valueOf(tokens.nextToken()));
					break;
				case ("bounds"):
					tCam.setBounds(Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()),
					           Double.valueOf(tokens.nextToken()));
					break;
				case ("res"):
					tCam.setRes(Integer.valueOf(tokens.nextToken()),
					            Integer.valueOf(tokens.nextToken()));
					break;
				case ("recursionlevel"):
					tRecLev = Integer.valueOf(tokens.nextToken());
					break;
				case ("ambient"):
					tAmbient = new Vector3D(Double.valueOf(tokens.nextToken()),
								Double.valueOf(tokens.nextToken()),
								Double.valueOf(tokens.nextToken()));
					break;
				case ("light"):
					Light light = new Light();
				    light.setP(Double.valueOf(tokens.nextToken()),
				    		   Double.valueOf(tokens.nextToken()),
				    		   Double.valueOf(tokens.nextToken()));
				    int lAtInf = Integer.valueOf(tokens.nextToken());
				    if (lAtInf == 0) {
				    	System.out.println("Light At Infinity, but ignoring this info!");
				    }
				    light.setE(Double.valueOf(tokens.nextToken()),
				    		Double.valueOf(tokens.nextToken()),
				    		Double.valueOf(tokens.nextToken()));
				    tLights.add(light);
					break;
				case ("sphere"):
				    Sphere sphere = new Sphere();
				    sphere.setC(Double.valueOf(tokens.nextToken()),
				    			Double.valueOf(tokens.nextToken()),
				    			Double.valueOf(tokens.nextToken()));
				    sphere.radius = Double.valueOf(tokens.nextToken());
				    sphere.setka(Double.valueOf(tokens.nextToken()),
				    			Double.valueOf(tokens.nextToken()),
				    			Double.valueOf(tokens.nextToken()));
				    sphere.setkd(Double.valueOf(tokens.nextToken()),
			    				Double.valueOf(tokens.nextToken()),
			    				Double.valueOf(tokens.nextToken()));
				    sphere.setks(Double.valueOf(tokens.nextToken()),
		    					Double.valueOf(tokens.nextToken()),
		    					Double.valueOf(tokens.nextToken()));

				    double kr_r = Double.valueOf(tokens.nextToken());
				    double kr_g = Double.valueOf(tokens.nextToken());
				    double kr_b = Double.valueOf(tokens.nextToken());
				    sphere.setkr(kr_r, kr_g, kr_b);
				    sphere.Ni = Double.valueOf(tokens.nextToken());
				    
				    double ko_r = kr_r;
				    double ko_g = kr_g;
				    double ko_b = kr_b;
				    double alpha = 16;
				    if (tokens.countTokens() > 2) {
				        ko_r = Double.valueOf(tokens.nextToken());
					ko_g = Double.valueOf(tokens.nextToken());
					ko_b = Double.valueOf(tokens.nextToken());
				    }

                                    if (tokens.hasMoreTokens()) {
					alpha = Double.valueOf(tokens.nextToken());
				    }

                                    sphere.setko(ko_r, ko_g, ko_b);
				    sphere.alpha = alpha;

				    tSpheres.add(sphere);
				    break;
				case ("model"):
					ModelObj model = new ModelObj();
					model.setWv(Double.valueOf(tokens.nextToken()),
								Double.valueOf(tokens.nextToken()),
								Double.valueOf(tokens.nextToken()));
					model.setTheta(Double.valueOf(tokens.nextToken()));
					model.setScale(Double.valueOf(tokens.nextToken()));
					model.setTv(Double.valueOf(tokens.nextToken()),
								Double.valueOf(tokens.nextToken()),
								Double.valueOf(tokens.nextToken()));
					
					model.setVsmooth(Double.valueOf(tokens.nextToken()));
					String modFile = tokens.nextToken();
					model.readObj(modFile);
					model.transformObj();
					model.transformTexture();
					tModels.add(model);
				default:
					break;
				}
			}
			read.close();
		} 
			
		catch (IOException e) {
			String errMessage = "Missing Input File: " + filename;
			System.err.println(errMessage);
			System.exit(0);
		}
			
		catch (NoSuchElementException e) {
			String errMessage = "Bad Token or Line in Input File";
			System.err.println(errMessage);
			System.exit(0);
		}
	}
	
	public Camera getCamera() {
		return this.tCam;
	}
	
	public ArrayList<Light> getLights() {
		return this.tLights;
	}
	
	public ArrayList<Sphere> getSpheres() {
		return this.tSpheres;
	}
	
	public ArrayList<ModelObj> getModels() {
		return this.tModels;
	}
	
	public Vector3D getAmbient() {
		return this.tAmbient;
	}
	
	public int getRecLev() {
		return this.tRecLev;
	}
}
