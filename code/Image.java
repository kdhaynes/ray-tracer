package code;


import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.FastMath;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Image {
	
	//Parameters
	private double myNCutoff = 0.96;
	private boolean printRayNums = false;
	
	//Camera Info
	private Camera myCamera;
	
	//Lights Info
	private int nLights;
	private double rayDToLight;
	private ArrayList<Light> lightList;
	private Light myLight;
	private Vector3D ambi, toL;
	
	//Ray Info
	private double px, py;
	private Vector3D Lv, Dv;
	
	//Sphere Info
	private int nSpheres;
	private ArrayList<Sphere> sphereList;
	private Sphere mySphere;
	
	//Model Info
	private int nModels;
	private ArrayList<ModelObj> modelList;
	private ModelObj myModel;
	
	//Shadow Info
	private boolean noShadowFlag;
	
	//Recursion Info
	private int nRecLevs;
	
	//Trace Info
	private double NdotL, CdotR;
	private Vector3D color, rcolor, tcolor;
	private Vector3D toC, spR;
	
	//Output RGB Info
	private int[][] rVals, gVals, bVals;
	private Vector3D newrgb;
	
	//Constructor
	public Image(DriverSpecs mySpecs) {
		this.myCamera = mySpecs.getCamera();
		myCamera.setUVW();
		
		this.lightList = mySpecs.getLights();
		this.nLights = lightList.size();
		this.sphereList = mySpecs.getSpheres();
		this.nSpheres = sphereList.size();
		this.modelList = mySpecs.getModels();
		this.nModels = modelList.size();
		
		this.ambi = mySpecs.getAmbient();
		this.nRecLevs = mySpecs.getRecLev();
		
		rVals = new int[myCamera.width][myCamera.height];
		gVals = new int[myCamera.width][myCamera.height];
		bVals = new int[myCamera.width][myCamera.height];
	}
	
	//Render
	public void render() {
		Ray myRay;
		for (int i=0; i<myCamera.width; i++) { 
			for (int j=0; j<myCamera.height; j++) {
				if (printRayNums) {System.out.println("RAY: " + i + " " + j);}
				myRay = pixel_ray(i, j, myCamera);
				newrgb = trace_ray(myRay, new Vector3D(0.0,0.0,0.0), new Vector3D(1.0,1.0,1.0), nRecLevs);
				rVals[i][j] = (int) Math.max(0, Math.min(255, Math.round(255.*newrgb.getX())));
				gVals[i][j] = (int) Math.max(0, Math.min(255, Math.round(255.*newrgb.getY())));
				bVals[i][j] = (int) Math.max(0, Math.min(255, Math.round(255.*newrgb.getZ())));
			}
		}
	}

	//Set up a ray from an image pixel
	public Ray pixel_ray(int i, int j, Camera cam) {
		px = (double)i/(double)(cam.width-1)*(cam.right-cam.left)+cam.left;
		py = (double)j/(double)(cam.height-1)*(cam.bottom-cam.top)+cam.top;
		Lv = cam.EV.add(cam.WV.scalarMultiply(cam.near));
		Lv = Lv.add(cam.UV.scalarMultiply(px));
		Lv = Lv.add(cam.VV.scalarMultiply(py));
		Dv = Lv.subtract(cam.EV);
		return new Ray(Lv, Dv);
	}
	
	//Find closest sphere
	public Sphere find_sphere(Ray myRay) {
		for (int s=0; s<nSpheres; s++) {
			myRay.sphereTest(sphereList.get(s));
		}
		return myRay.best_sph;
	}
	
	//Find closest object
	public ModelObj find_model(Ray myRay) {
		for (int m=0; m<nModels; m++) {
			myRay.modelTest(modelList.get(m));
		}
		return myRay.best_mod;
	}
	
	//Trace the ray
	public Vector3D trace_ray(Ray myRay, Vector3D accum, Vector3D refatt, int level) {
		
		Vector3D N;
		Vector3D hitPt, hitKa, hitKd, hitKs, hitKr, hitKo;
		double hitAlpha, hitNi;
		int hitIllum;
		String hitObj;
		
		//If the Ray is null, return the already accumulated color vector
		if (myRay == null) {
			return accum;
		}
		
		//Test to see if sphere and/or model hit with ray
		myModel = find_model(myRay);
		mySphere = find_sphere(myRay);
		if ((mySphere == null) && (myModel == null)) {
			return accum;
		}
		
		//We know we've hit something...
	    if (myRay.best_st < myRay.best_mt) {
	    	//Sphere is closer
	    	N = myRay.best_spt.subtract(myRay.best_sph.C).normalize();
		    hitPt = myRay.best_spt;
		    hitKa = mySphere.ka;
		    hitKd = mySphere.kd;
		    hitKs = mySphere.ks;
		    hitKr = mySphere.kr;
		    hitKo = mySphere.ko;
		    hitAlpha = mySphere.alpha;
		    hitIllum = 3;
		    hitNi = mySphere.Ni;
		    hitObj = "sphere";
	    } else {
	    	//Model is closer
	    	N = myRay.best_mptN;
			hitPt = myRay.best_mpt;
			int matRef = myRay.best_mmat;
			Material hitMaterial = myModel.getMaterial();
			hitKa = hitMaterial.getKa(matRef);
			hitKd = hitMaterial.getKd(matRef);
			hitKs = hitMaterial.getKs(matRef);
			hitKr = hitMaterial.getKs(matRef);
			hitKo = hitMaterial.getKo(matRef);
			hitAlpha = hitMaterial.getNs(matRef);
			hitIllum = hitMaterial.getIllum(matRef);
			hitNi = hitMaterial.getNi(matRef);
			hitObj = "model";
			
			Texture hitTexture = hitMaterial.getMatTexture(matRef);
			if (!hitTexture.textureName.equals("")) {
				int[] uvPt = myModel.getTexturePt(myRay.best_mfindx, 
						hitPt.toArray(), hitTexture.nimx, hitTexture.nimy);
				hitKo = hitTexture.getKo(uvPt[0],uvPt[1]);
				hitKd = hitKo;
			}
	    }
	    double hitSumKo = hitKo.getX() + hitKo.getY() + hitKo.getZ();
	    Vector3D hitTr = new Vector3D(1.-hitKo.getX(), 1.-hitKo.getY(), 1.-hitKo.getZ());
		 
		//Ambient illumination
		color = smultiply(ambi,hitKa);
		   
		//Loop through lights to get color
		for (int lt=0; lt<nLights; lt++) {
			myLight = lightList.get(lt); 
				
			//Shadows 
			toL = myLight.P.subtract(hitPt);
			Ray rayToLight = new Ray(hitPt, toL);
			rayDToLight = toL.dotProduct(rayToLight.D);
			find_sphere(rayToLight);
			find_model(rayToLight);
			if ((rayToLight.best_st < rayDToLight) || (rayToLight.best_mt < rayDToLight)) {
				noShadowFlag = false;
			} else {
				noShadowFlag = true;
			}
			
			//Diffuse and specular reflection
			toL = toL.normalize();
			if (hitObj.equals("sphere") ) {
				NdotL = N.dotProduct(toL);
			} else {
				NdotL = N.dotProduct(toL);
				if (NdotL < 0.) {
					NdotL = N.negate().dotProduct(toL);
				}
			}
				
			if (noShadowFlag) {
				if (NdotL > 0.00001) {
					tcolor = smultiply(hitKd, myLight.E);
					color = color.add(tcolor.scalarMultiply(NdotL));
					toC = myRay.L.subtract(hitPt).normalize();
					spR = N.scalarMultiply(2*NdotL).subtract(toL).normalize();
					CdotR = toC.dotProduct(spR);
					if (CdotR > 0.00001) {
						rcolor = smultiply(hitKs, myLight.E);
						color = color.add(rcolor.scalarMultiply(FastMath.pow(CdotR,hitAlpha)));
					}
				}
			}
		}
		accum = accum.add(smultiply3(refatt, color, hitKo));
		
		//Recursive Ray Tracing
		if (level > 0) {
			//Reflection
			Vector3D flec = new Vector3D(0.0, 0.0, 0.0);
			Vector3D Uinv = myRay.D.negate();  
			Vector3D refR = N.scalarMultiply(2.0 * N.dotProduct(Uinv)).subtract(Uinv).normalize();
			if ((hitObj == "sphere") || (refR.dotProduct(myRay.D) < myNCutoff)) {
				Ray newRay = new Ray(hitPt, refR);
				if ((hitIllum == 3) || (hitIllum == 6)) {
					flec = trace_ray(newRay, flec, smultiply(hitKr, refatt), (level - 1));
					accum = accum.add(smultiply3(refatt, hitKo, flec));
				}
			}
		}

		//Refraction
		if ((level > 0) && (hitSumKo < 3.0)) {
			Vector3D thru = new Vector3D(0.0, 0.0, 0.0);
			Ray fraRay = null;
			
			if (hitObj.equals("sphere")) {
				fraRay = myRay.best_sph.refractR(myRay.D.negate(), hitPt, hitNi, 1.0);
			} 
			if ((hitObj.equals("model")) && (hitIllum == 6)) {
				fraRay = myRay.best_mod.refractR(myRay.D.negate(), myRay.best_mptN, hitPt,
						myRay.best_mfindx, hitNi, 1.0);
			}
			
			if (fraRay != null) {
				thru = trace_ray(fraRay, thru, smultiply(hitTr, refatt), (level - 1));
				accum = accum.add(smultiply3(refatt, hitTr, thru));
			}
		}

		return accum;
	}
	
	//Multiply two vectors
	public Vector3D smultiply(Vector3D v1, Vector3D v2) {
		Vector3D vout = new Vector3D(v1.getX()*v2.getX(),
				v1.getY()*v2.getY(), v1.getZ()*v2.getZ());
		return vout;
	}

	//Multiply three vectors
	public Vector3D smultiply3(Vector3D v1, Vector3D v2, Vector3D v3) {
		Vector3D vout = new Vector3D(v1.getX()*v2.getX()*v3.getX(),
				v1.getY()*v2.getY()*v3.getY(), v1.getZ()*v2.getZ()*v3.getZ());
		return vout;
	}
	
	//Write out the image
	public void write(String outfile) {
		try {
			BufferedWriter writeout = new BufferedWriter(new FileWriter(outfile));
			writeout.write("P3\n");
		
			String outline = String.format("%d %d 255\n", myCamera.width, myCamera.height);
			writeout.write(outline);
	
			for (int j=0; j<myCamera.height; j++) {
				for (int i=0; i<myCamera.width; i++) {
					outline = String.format("%d %d %d ", 
							rVals[i][j], gVals[i][j], bVals[i][j]);
					writeout.write(outline);
				}
				writeout.newLine();
			}
			writeout.close();
		} 
		
		catch (IOException e) {
			String errMessage = "Error Writing File: " + outfile;
			System.err.println(errMessage);
			System.exit(0);
		}
	}
}

