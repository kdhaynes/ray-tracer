package code;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;


public class Material {

	private int matNum;
	private String matNow;
	private ArrayList<String> matName;
	private ArrayList<Integer> matIllum;
	private ArrayList<Double> matNs, matNi, matD;
	private ArrayList<Vector3D> matKa, matKd, matKs, matKe, matKo;
	private ArrayList<Texture> matTexture;
	
	// Constructor
	public Material() {
		matName = new ArrayList<String>();
		matIllum = new ArrayList<Integer>();
		matNs = new ArrayList<Double>();
		matNi = new ArrayList<Double>();
		matD = new ArrayList<Double>();
		matKa = new ArrayList<Vector3D>();
		matKd = new ArrayList<Vector3D>();
		matKs = new ArrayList<Vector3D>();
		matKe = new ArrayList<Vector3D>();
		matKo = new ArrayList<Vector3D>();
		matTexture = new ArrayList<Texture>();
	}

	// Get Routines
	public int getMatRef(String myName) {
		return matName.indexOf(myName);
	}
	
	public Texture getMatTexture(int mindx) {
		return matTexture.get(mindx);
	}
	
	public Vector3D getKa(int mindx) {
		return matKa.get(mindx);
	}
	
	public Vector3D getKd(int mindx) {
		return matKd.get(mindx);
	}
		
	public Vector3D getKo(int mindx) {
		return matKo.get(mindx);
	}
	
	public Vector3D getKs(int mindx) {
		return matKs.get(mindx);
	}
	
	public double getNi(int mindx) {
		return matNi.get(mindx);
	}
	public double getNs(int mindx) {
		return matNs.get(mindx);
	}

	public int getIllum(int mindx) {
		return matIllum.get(mindx);
	}
	
	// Read Material File
	//Read in the model object file
	public void readMat(String dir, String file) {
		try {			
			BufferedReader read = new BufferedReader(new FileReader(dir + file));
			String line = null;
			double kx, ky, kz;
			while ( (line = read.readLine()) != null) {
				StringTokenizer tokens = new StringTokenizer(line);
				String first = "";
				if (tokens.hasMoreTokens()) {
					first = tokens.nextToken();
				}
				switch (first) {
				case ("newmtl"):
					matNum ++ ;
				    if (tokens.hasMoreTokens()) {
				    	matNow = tokens.nextToken();
				    	matName.add(matNow);
				    }
				    matKo.add(new Vector3D(1.0, 1.0, 1.0));
				    matTexture.add(new Texture(""));
					break;
				case ("Ns"):
					matNs.add(Double.valueOf(tokens.nextToken()));
					break;
				case ("Ka"):
					kx = Double.valueOf(tokens.nextToken());
					ky = Double.valueOf(tokens.nextToken());
					kz = Double.valueOf(tokens.nextToken());
					Vector3D myKa = new Vector3D(kx,ky,kz);
					matKa.add(myKa);
					break;
				case ("Kd"):
					kx = Double.valueOf(tokens.nextToken());
					ky = Double.valueOf(tokens.nextToken());
					kz = Double.valueOf(tokens.nextToken());
					Vector3D myKd = new Vector3D(kx,ky,kz);
					matKd.add(myKd);
					break;
				case ("Ks"):
					kx = Double.valueOf(tokens.nextToken());
					ky = Double.valueOf(tokens.nextToken());
					kz = Double.valueOf(tokens.nextToken());
					Vector3D myKs = new Vector3D(kx,ky,kz);
					matKs.add(myKs);
					break;
				case ("Ke"):
					kx = Double.valueOf(tokens.nextToken());
					ky = Double.valueOf(tokens.nextToken());
					kz = Double.valueOf(tokens.nextToken());
					Vector3D myKe = new Vector3D(kx,ky,kz);
					matKe.add(myKe);
					break;
				case ("Tr"):
					kx = Double.valueOf(tokens.nextToken());
					ky = Double.valueOf(tokens.nextToken());
					kz = Double.valueOf(tokens.nextToken());
					Vector3D myKo = new Vector3D(1-kx, 1-ky, 1-kz);
					matKo.set(matNum-1, myKo);
					break;
				case ("Ni"):
					matNi.add(Double.valueOf(tokens.nextToken()));
					break;
				case ("d"):
					matD.add(Double.valueOf(tokens.nextToken()));
					break;
				case ("illum"):
					matIllum.add(Integer.valueOf(tokens.nextToken()));
					break;
				case ("map_Kd"):
					Texture matTxt = new Texture(dir + tokens.nextToken());
				    matTexture.set(matNum-1, matTxt);
				default:
					break;
				}
			} 
			read.close();
		}
		
		catch (IOException e) {
			String errMessage = "Missing Material File: " + file;
			System.err.println(errMessage);
			System.exit(0);
		}
			
		catch (NoSuchElementException e) {
			String errMessage = "Bad Token or Line in Material File";
			System.err.println(errMessage);
			System.exit(0);
		}
	}
}
