JFLAGS = 
JC = javac -classpath .:lib/commons-math3-3.6.1.jar:lib/commons-io-2.6.jar

JVM = java
FILE =

.SUFFIXES: .java .class

.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	code/Camera.java \
	code/DriverSpecs.java \
	code/Image.java \
	code/Light.java \
	code/ModelObj.java \
	code/Ray.java \
	code/Sphere.java \
	code/Texture.java \
        code/Raytracer.java

MAIN = Raytracer

default: classes

classes: $(CLASSES:.java=.class)

run: $(MAIN).class
	$(JVM) $(MAIN)

clean:
	$(RM) code/*.class